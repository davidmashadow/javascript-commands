# Javascript commands

Within the examples, the architecture of Javascript, in which areas it is used, all its features, all methods, all functions and object-oriented programming methods from scratch to the most advanced level are covered in the most detailed and abundant

# Eğitim Amacı
Sıfırdan ileri seviyeye javascript becerilerinin kazandırılması amaçlanmıştır.

# Eğitim Özeti
Sürekli gelişen web teknolojilerinin en popüler ve en olmazsa olmaz dillerinden birisi de Javascript'tir.
Eğitim dahilinde sıfırdan en ileri seviyeye kadar Javascript'in mimarisini, hangi alanlarda kullanıldığı, tüm özelliklerini, tüm metotlarını, tüm fonksiyonlarını ve nesne tabanlı programlama yöntemlerini en ayrıntılı şekilde ve bol örneklerle işlenmektedir.

# Hedef Kitle
Javascript ile sıfırdan ileri düzeye kadar mükemmel dinamik web siteleri geliştirmek isteyen kişiler.
Durağan web sayfalarına dinamiklik kazandırmak isteyen kişiler.
Web sayfaları dahilinde anlık işlemler yapmak isteyen kişiler.
Tüm HTML elementlerine ekti etmek isteyen kişiler.
Browser (tarayıcı) oyunu kodlamak isteyen kişiler.
Game Engine (oyun motoru) programlarını kullanırken Javascript ile kod yazmak isteyen kişiler.

# Gereksinimler
Temel algoritma bilgisi
Temel html ve css bilgisi

# Kazanımlar
Dinamik web site tasarlayabilir,
Javascript ile en yeni web teknolojilerini kullanarak web site yapabilir,
Javascript ile oyun, dinamik içerikler, slaytlar, olaya duyarlı sayfalar geliştirebilir.